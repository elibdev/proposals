# Bug Report Format

## Proposed Interface

Every bug report should be recorded in a standardized way.
It should be clear how to create a new bug report and clear to read.
Having a collection of all the fixed bugs should be helpful for learning.
Similar to a proposal, an bug report should be written in readable language.

## Benefits

Bug report templates are common in open source development, because they
encourage people to provide enough information to be able to fix the issue.
By adding information about how the issue was fixed we can create a reference
for when a similar issues come up later.

## Drawbacks

It should be really easy to report an issue.
Requiring someone to go through a complex process discourages them.
We should make it as easy as possible to create an bug report.

## Possible Alternatives

We could just use Github issues for this too.
Much like with proposals, the conversational nature of Github issues
makes it less useful as a reference to review later on.
You can't (and shouldn't) edit a conversation for clarity.
This way we can edit the report but keep the unedited conversation.

## Implementation Ideas

We can use the following basic format:

1. __Bug Summary:__ What bug did you experience?
2. __Steps to Reproduce:__ How can this bug be reproduced?
3. __Expected Results:__ What did you expect to happen?
4. __Actual Results:__ What actually happened?
