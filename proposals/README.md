# Interface Proposals for Formal Web

This repository contains a collection of proposal documents
for developing Formal Web collaboratively.

Each document should propose an interface to one component.
The proposal should describes the purpose of the component,
how it can be used, and how it should behave.
If it would make sense to separate one proposal into multiple,
you should separate them to keep the documents readable.
Proposals should be written in clear, concise, simple language.
They are technical specifications, but they should be readable.

A proposal should be:

- general enough to be implemented by different people with different technologies
- specific enough to facilitate interoperability between implementations

You can use the [example proposal document](example-proposal.md) as a starting point.

## In Progress

- [Interface Proposal Format](proposal-format.md)
- [Bug Report Format](bug-reports.md)
- [Python Markdown Documentation](python-markdown-docs.md)
- [Website Server](website-server.md)
- [Website Host](website-host.md)
- [Database](database.md)
