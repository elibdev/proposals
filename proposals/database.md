# Database

## Proposed Interface

There are components of Formal Web that will need to store data.
We should offer a database abstraction that feels natural
to use, performs well, and requires minimal configuration.

### Database Structure

The database is divided into tables.
Tables are named groups of sorted key value pairs.
The database should encode and decode keys and values
automatically using the Msgpack encoding.

The database should ensure that all reads and writes to the
database occur within transactions that are always closed.

## Benefits

Having a simple language-neutral transactional database
with a natural interface should make it straight-forward
to extend the functionality of the database.
The flexible table format is meant to be a compromise
between rigid relational schemas and key value stores.

In relational databases you sometimes end up needing
to include procedural logic in your queries,
but you can't get underneath the SQL implementation
to get maxmium efficency. 

Utilizing an embedded database means that only
a local file on disk is used to store the database.
This provides flexibility working with multiple databases.

## Drawbacks

The interface will be new to people.
Many people already know SQL, and some
would rather just reuse those skills. 

## Possible Alternatives

We could instead use a relational database like PostgreSQL or
SQLite that uses SQL (Structured Query Language) to control it.

## Implementation Ideas

This can be implemented using LMDB, an efficient embedded
transactional sorted key value store,
It supports inner databases, which we can use for tables.

