
# Website Host

## Proposed Interface

A website host offers an interface for creating and managing
multiple websites and users on the same server.

This proposal builds on the
[website server proposal](website-server.md).

### Domains

A website host must have a domain that it is accessible from.
Every website must have a name that is URL-safe
and unique for that host.

Let's say that our website host uses the domain `host.com`,
and our website is named `coolsite`.
The website will be available at `coolsite.host.com`.
Every web page will include a canonical link to the
same page at its custom domain if it is set.
This will work if you create a `CNAME` DNS record
pointing to `coolsite.host.com` from your custom domain.

### Preview and Publish

Any changes to the files to a mounted WebDAV drive (web drive)
are immediately visible at `coolsite.host.com/preview`
if the user is authenticated and authorized.
The changes can be published using a button on the preview site.

### User Administration

A host has a user database where each user has:

- an email (verified or unverified)
- a salted passphrase hash
- a list of valid authentication tokens
- possibly a one-time password for forgot passphrase

There must be one or more admin users that have
permissions to perform administrative actions.
Regular users only have permissions to edit their websites.
Setting up a host requires creating one admin user.

A user can have admin privileges for certain sites,
and/or for the website host itself.

A non-admin user can perform the following actions:

- make unpublished changes to the website content
- create a new site where they are the site admin
- export website

A site admin user can perform these additional actions:

- publish changes
- rollback changes to a previous published version
- invite a user to the site
- remove a user from the site

A host admin user can perform these actions:

- remove an existing site
- remove an existing user

A user can request a one-time password
that only lasts for an hour to their email.
The user can change their passphrase once they log in.
Changing the passphrase invalidates all tokens.

## Benefits

Offering a single user database that works across all websites
on a host means that users don't need an account for every site.
A user's account at the website host will work for all its sites.

It is rare for a single website to efficiently utilize the
resources of a single server. 
The spiky traffic of websites doesn't fit neatly into
the square box of a VM (Virtual Machine).
Serving multiple sites from the same server
should help balance resource utilization, since it will
likely be different for different sites. 

When we introduce monitoring and metrics, all of those
will be visible for each website in one place. 

## Drawbacks

A website host for multiple websites will introduce
technical complexity over having no interface at all.

## Possible Alternatives

We could build an administration interface for a single site instead.
In our experience, somebody with only one website does
not want to think about hosting or administration.
An administration interface for a single website is very limited,
because you will not have the benefit of being able to manage
multiple sites and users in one system.

We could also build on some type of generic pre-existing server
administration interface for web applications.

Another option would be to separate the website server and the
website host into separate modules that just interact.
The website host would create and manage instances of the
website server using its documented interface.

This modularization makes sense, and is probable where we're headed.
The interface for managing a single website is not entirely clear
now, but we should learn more about it by implementing the website host.
We will probably be able to design a better interface after we
implement the website host and learn lessons from it.
This website host interface could be compatible with this modularization.

## Implementation Ideas

[Go has a WebDAV server implementation in its standard library][godav],
so that makes it a very attractive language to implement the website
host with, in addition to its speed and great networking libraries.
There's also a markdown library [blackfriday][gomd] that looks solid.

[godav]: https://godoc.org/golang.org/x/net/webdav
[gomd]: https://github.com/russross/blackfriday

Python is a more approachable language in many ways, so it might
invite more contributions and extensions to the code.
There is a [WSGI-compatible Python WebDAV server][pydav],
and there are many markdown libraries.

[pydav]: https://github.com/mar10/wsgidav

A website host written in Go would probably be easier to deploy
than a Python implementation since it would be self-contained.
There are probably more people interested in running the
website host than extending it, so Go might make more sense.
A Python website host would need to be deployed behind a
reverse proxy server like [Nginx.](https://www.nginx.com/)
Go applications can be run in production by themselves.
