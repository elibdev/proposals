# Interface Proposal Format

## Proposed Interface

Use a standardized format for proposing interfaces that can be
compatible across different implementations.

A proposal should be:

- general enough to be implemented by different people with different technologies
- specific enough to facilitate interoperability between implementations

## Benefits

Formal Web is intended to be an open system technically and socially.
Anyone should be able to keep up with or participate in the
development process of Formal Web.
We should use an open and standardized process that all changes go through.

Formal Web should use a process of community proposals similar to RFCs.
[RFCs (Requests For Comments)](https://ietf.org/standards/rfcs/)
are collaborative documents that collectively specify the internet and web.
This process has worked very well to enable the cross-compatible
system of information technologies we have today.

Interfaces are where components in systems meet each other.
An interface is an abstract description of how a component
should behave and connect to the world outside of it.

Implementors should be able to make their own decisions
and use whatever technologies they want to use.

A standard interface proposal format should make the project much more 
accessible to contributors by increasing transparency and access points.
It should also make the technical and design decisions behind Formal Web
more visible to people using but not directly developing the project.

Writing out proposals for each change ensures that all interfaces
are specified clearly and gives an opportunity for people to comment.
Implementation is hard, and it's best to get input before that phase.
Proposals are for communication and discussion, not for gatekeeping.
Anyone can write a proposal, and anyone can implement a proposal.

## Drawbacks

Many people who like coding or designing don't like writing.
Introducing a process that requires writing might discourage those people.

Collaboration requires communication, and written communication is
one of the most universally accessible methods.

Formal Web is intended to be globally useful, and having a collection
of English written documents is a barrier for non-English speakers.
We should translate the documents as soon as possible.

Technical documents also have a tendency to be unreadably dense.
There is the risk of introducing a barrier to participation if
the proposals are written in a way in that makes people fall asleep.
We should focus on editing for clear and plain language and concision.
Having short and bite-sized proposals should help with this also.

## Possible Alternatives

We could just use Github issues and have the discussions there.
The thread-based format of Github issues makes it easy to participate in,
but difficult to read through to get a sense for the decisions made.

Github issues are similar to mailing lists how they read like conversations.
Conversations are important for working through things together, but
even more important is communicating the decisisons made to other people.
Decisions should be made in public, and they should be written down.
Having a standard format should make it easy to find what you're looking for.

## Implementation Ideas

Proposals should be written in clear, concise language.
Each proposal should describe one interface.

Proposals can use the following format (which this document demonstrates):

1. __Proposed Interface:__ Briefly, what is the interface you're proposing?
3. __Benefits:__ How would this new interface benefit the people affected?
4. __Drawbacks:__ What would be the drawbacks to the people affected?
5. __Possible Alternatives:__ What are some possible alternatives to this interface?
6. __Implementation Ideas:__ Do you have ideas for how this could be implemented?