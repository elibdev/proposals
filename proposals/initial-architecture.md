# Initial Architecture and Implementation

## Proposed Interface

We should create a basic plan for the architecture and the implementation
of the first iteration of Formal Web.

## Benefits

By documenting the motivations behind the architectural design of Formal Web
we can have a reference for future discussions about architecture.
If the context surrounding this proposal changes,
a different architecture might make more sense.

## Drawbacks

We should keep our ideas about how to structure Formal Web flexible,
so that they can adapt as our understandings of people's needs grows.
This initial architecture is not yet informed by product feedback,
so it would be good to revisit the architecture after we have that.

## Possible Alternatives

We could just create the implementation without a plan.

## Implementation Ideas

Formal Web should have all of the components needed for building web apps.
The components should work well together and have a small interface.
Doing things the standard way should require very little knowledge about
the architecture of Formal Web.
When people need more customization, they should only have to know about
the component they want to customize.
We should design the system so that knowledge about it can be easily
transferred to other similar systems.

The division of the architecture can allow for customization
for people at all levels of learning web development.

- __Web Design:__ modify templates and stylesheets
- __Databases:__ modify database actions
- __HTTP__: modify request handlers and routes
- __Deployment:__ manage deployment of web apps

### Differences from Web Frameworks like Django or Rails

Web frameworks like Django and Rails are built for coders.
To get even a basic test project running, you need to write code.
Formal Web's architecture is similar to, and inspired by the
MVC (Model, View, Controller) architecture used by Django and Rails.
Although Formal Web is fully customizable through coding like web
frameworks are, it is designed to be managed by people who don't code.

This makes Formal Web more similar to Wordpress in a certain way,
because it also includes a graphical interface for end users.
Django includes an administrator interface, but it's not for end users.
We want Formal Web to provide high-level abstractions that work together
seamlessly while still keeping the foundation modular and customizable.

### Python as the Programming Language

Python is a readable, approachable and practical language.
There are Python libraries for almost anything you'd want to do.
Although other languages are faster or better at things
like concurrency, Python has the broadest community.
Since we are striving for accessibility, Python is a good match.

We should try to design the programming interface so that
basic Python coding skills is all you need to customize your app.

### Library Structure

- `actions/`: Actions define possible read or write patterns on the database.
- `templates/`: Templates define HTML building blocks of pages and how they combine.
- `apps/`: Apps connect database actions, templates and HTTP routes.

### LMDB as the Database

At the center of almost every web service is a database.
Many databases use SQL (Structured Query Language), 
which is another programming language for people to learn.
Key-value databases have become popular because of their simplicity and speed.
A key-value database stores a mapping of key data to value data.
Most relational databases use key-value stores for each table,
so relational databases are actually built on top of them.
Relational databases are difficult to optimize for particular use cases,
while key-value databases allow more direct control over data access.
Key value databases also can have flexible schemas,
which suits Python's dynamic typing well.

A key-value store corresponds with the programming concept of a dictionary,
so in most programming languages is a very intuitive structure to work with.
For these reasons it makes sense to use a key-value store for the database.
[LMDB](http://www.lmdb.tech/doc/) is a very fast transactional key-value
database with support for nested databases stored at keys.
There's also [a great LMDB Python binding](https://lmdb.readthedocs.io).
We can provide a wrapper that provides a dict-like transaction object.
That way you don't need to learn a new database library.

### Database Actions

We can provide a collection of database actions that can be combined
in the same database to enable people to only store the data they want.
The database actions should provide out-of-the-box support for common
usage patterns like storing users and posts.

Actions are simply functions that operate on a database transaction.
They can only interact with each other by modifying the same data.
They are responsible for validating the data that they store.
One advantage of this design is that we can expose only the data
access patterns that we know are going to efficient and useful.

This model feels simpler than ORM (Object Relational Manager) systems
usually associated with the MVC (Model, View, Controller) architecture.
With MVC you need to define the schema in the model layer
and the access patterns separately in the controller layer.
The model layer is not as useful without the controller layer,
since it's external inteface doesn't fully define usage patterns.

Formal Web's actions replace both models and controllers.
Database actions can be added or removed individually on an existing database,
while models require complex and dangerous migrations when the schema changes. 
Actions are more loosely coupled than models, which feels better suited to
Formal Web since it's easier to change in small pieces without breaking things.

Data should be validated at the action layer, and invalid data should raise
an exception with information about how the data is invalid for the action.

### Serialization Format for Stored Values

There are a few data formats we've considered:

- [JSON (JavaScript Object Notation)](https://json.org)
- [MessagePack](https://msgpack.org/)
- [XML (eXtensible Markup Language)](https://developer.mozilla.org/en-US/docs/XML_introduction)
- [Protocol Buffers](https://developers.google.com/protocol-buffers/)

JSON and XML are text-based serialization formats that are used across HTTP.
HTTP is limited to text-based formats, but databases can store binary formats.
MessagePack is like a binary version of JSON and can store data types commonly
used in dynamic languages like maps and arrays.
Protocol Buffers is kind of like a binary XML.

JSON and MessagePack feel well-suited for dynamically typed languages
because there isn't a separate step of declaring your schema.
XML and Protocol Buffers involve first creating strongly typed schemas,
so they feel better suited for statically typed languages.

Since we are using a dynamic language like Python with a database that
supports binary formats, MessagePack feels like the most natural choice.

### Templates

Templates are parameterized blocks of HTML that can be combined or extended.
Formal Web should include templates needed for a basic web site.
Templates should be accessible and SEO-friendly semantic HTML.

### Apps

Apps provide an HTTP interface to the database actions.

Each HTTP request automatically opens a database transaction.
If the HTTP method is POST, PATCH, or DELETE, it will open a write transaction.
Any other HTTP method like GET or HEAD will open a read-only transaction.
This transaction object is available on the request object.

If an action raises an exception, that will automatically return an HTTP error.

Apps define how HTTP requests are mapped to database actions. Each app includes:

- a dict mapping routes to request handlers
- request handlers that use actions and templates
