# Example Interface Proposal

You can use this document as a starting point for writing new proposals.

## Proposed Interface

Briefly, what is the interface you're proposing?

## Benefits

How would this new interface benefit the people affected?

## Drawbacks

What would be the drawbacks to the people affected?

## Possible Alternatives

What are some possible alternatives to this interface?

## Implementation Ideas

Do you have ideas for how this could be implemented?
