# Website Server

## Proposed Interface

Use WebDAV and Markdown to offer a simple website
editing interface with mountable cloud drives
and plain folders of text and media files.

A user's website content consists of the collection
of files in their WebDAV directory.
Standard filetypes are served normally.
Markdown files are parsed and displayed using the theme.

### Site Server Object

There should be a site object that can be used to create and
run a website server programmatically.
We will call this object `Site`.

`Site` should have the following properties that must be
configured before it can be run:

- `address`: the domain and port to serve the site from
- `dir`: the local directory where the website is stored
- `cert`: the path of the TLS certificate to use
- `key`: the path of the TLS key to use

`Site` should offer the following methods:

- `start`: run the pre-configured site server
- `stop`: stop a running site server

`Site` only runs over HTTPS using port 443.
There is no HTTP option, but you can use a self-signed
TLS certificate and key for development.

### Serving Static Files

Files can be served normally at their path location.
HTML files can be served with the `.html` extension removed.
Each markdown file can be rendered as HTML at the path
location with the `.md` extension removed.
A file named `index.md` or `index.html` will be served at `/`.

If a `.html` and `.md` file exist with the same name in the
same directory, the `.html` file will be served.
This is important because the `.md` and the rendered `.html`
files will be both included in the `live/` directory.
This is so that exports have both formats.

### Site Storage

All data for the website is stored in its root directory.
Inside the `dir` directory are two folders:

- `draft/`: the draft version of all the files
- `live/`: the last published version of all files

Only `draft/` is avaialable over WebDAV.
When you publish your drafts, the contents of `draft/` are copied into `live/`, replacing the previous contents.
The live website is served from `live/`.

### Import and Export

All data can be exported or imported at any time as a zip file.
A user must be an admin of the site to export or import its data.
Importing new data will replace all existing data, so be careful.

A site doesn't perform backups itself, but it does enable
the user to perform their own backups. 
You should regularly (preferably automated) export your site's
data and copy it to a secure location to back it up.
You can restore your site to a previous version by
importing a previously exported zip file.

### Markdown Page Format

The content is normal Markdown.
It must include one and only one `# Heading` (`<h1>`).
The content of the `<h1>` tag is used as the page title.

The first image on the page (if any) is used as the cover image
used for social sharing platforms.

### Site Theme

The site theme is a combination of an HTML template
and a CSS stylesheet that style the webpages.

The files should be named `theme.html` and `theme.css`,
and they should be placed in the site's root directory.
The `theme.html` should have a `<main>` element.
The generated HTML from markdown pages will go in there.

### Editing and Publishing

You can edit your website by mounting it as a WebDAV drive
on your device that looks like a local storage device.
The WebDAV drive is the only editing interface,
but it only edits drafts.

You can log in to preview your edits and publish anytime
at `/preview`.
The edits will go live when you publish.

## Benefits

Using the combination of Markdown and WebDAV makes
it possible for people to create websites without
introducing any additional tools into their workflow.
It feels just like working with files on Dropbox.

WebDAV is integrated into most operating systems natively.
If you know how to manage files on your computer,
you'll know how to manage files on your website.
Pages and media feel just like local files.

Markdown is just text files, which are supported on all platforms. 

Markdown also offers the capabilities of HTML without the
editing headache. 

In our subjective research we've found that people often
think about websites in terms of "pages", not "posts."
More people want websites than blogs.
Using Markdown naturally aligns with that way of thinking
about websites, since one Markdown file is simply one page.

## Drawbacks

Markdown syntax can be confusing for beginners. 
You can't see what the page will look like while editing.
This can be partially addressed by showing a rendered preview.
Not all users are familiar with WebDAV and how to use it,
although it is just like Dropbox or iCloud.

## Possible Alternatives

We could create our own editing interface instead of Markdown,
or use an open source component for it.
We could also use a different markup language than Markdown,
even though Markdown seems like the crowd favorite.
We could use SFTP instead of WebDAV, but it's less user-friendly.

## Implementation Ideas

The implementation should have the following properties:

- fast static file serving
- reliable WebDAV server component
- secure up-to-date TLS
- reliable Markdown parsing

All of these properties except for Makrkdown parsing
can be handled efficiently using Nginx as a reverse proxy.
This would only need to be supplemented with a markdown parser.
